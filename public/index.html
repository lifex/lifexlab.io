<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

  <link rel="icon" type="image/x-icon" href="icons/icon-32x32.png">

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link
    href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
    rel="stylesheet">

  <link rel="stylesheet" href="style.css">

  <script src="https://kit.fontawesome.com/d490ac4e1c.js" crossorigin="anonymous"></script>

  <title>lifex</title>
</head>

<body>

  <div id="wrapper-centered">
    <div id="header">
      <a href="index.html">
        <img src="images/lifex_logo.png" alt="Lifex Logo" style="width: 100%; height: auto;">
      </a>
      <p>
        <strong class="full-text">A finite element HPC library for simulating the cardiac function</strong>
        <strong class="short-text">An HPC library for cardiac simulations</strong>
      </p>
    </div>

    <div id="info">
      <p>
        <strong>life<sup>x</sup></strong> is a C++ software library for finite
        element simulations of the cardiac function, focusing on
        <strong>flexibility</strong>, <strong>user-friendliness</strong>, and
        <strong>parallel scalability</strong> for high-performance computing. It
        was developed at <a href="https://mox.polimi.it/">MOX, Dipartimento di
          Matematica, Politecnico di Milano</a>, and relies on
        <a href="https://www.dealii.org/">deal.II</a> for finite element
        calculations.
      </p>
    </div>

    <a name="modules"></a>
    <h1>Released modules</h1>
    <div id="cards-wrapper">
      <div class="card left">
        <p class="card-header">life<sup>x</sup>-core</p>
        <p class="card-description">an open-source framework for multiphysics and multiscale problems</p>
        <img class="card-figure" src="images/core/cahnhilliard.jpg"></img>
        <div class="card-footer">
          <a class="click-icon fa-solid fa-file-lines tooltip"
            href="https://arxiv.org/abs/2411.19624">
            <span class="tooltiptext">release paper</span>
          </a>
          <a class="click-icon fa-solid fa-code-branch tooltip" href="https://gitlab.com/lifex/lifex-public">
            <span class="tooltiptext">source code</span>
          </a>
          <a class="click-icon fa-solid fa-ellipsis tooltip" href="core.html" style="position: absolute; right: 20px">
            <span class="tooltiptext">read more</span>
          </a>
        </div>
      </div>

      <div class="card right">
        <p class="card-header">life<sup>x</sup>-cfd</p>
        <p class="card-description">an easy-to-use open-source tool for cardiovascular CFD simulations</p>
        <img class="card-figure" src="images/cfd/la-vorticity.png"></img>
        <div class="card-footer">
          <a class="click-icon fa-solid fa-file-lines tooltip" href="https://doi.org/10.1016/j.cpc.2023.109039">
            <span class="tooltiptext">release paper</span>
          </a>
          <a class="click-icon fa-solid fa-download tooltip" href="https://doi.org/10.5281/zenodo.10041834">
            <span class="tooltiptext">download binaries and examples</span>
          </a>
          <a class="click-icon fa-solid fa-code-branch tooltip" href="https://gitlab.com/lifex/lifex-public">
            <span class="tooltiptext">source code</span>
          </a>
          <a class="click-icon fa-solid fa-ellipsis tooltip" href="cfd.html" style="position: absolute; right: 20px">
            <span class="tooltiptext">read more</span>
          </a>
        </div>
      </div>

      <div class="card left">
        <p class="card-header">life<sup>x</sup>-fiber</p>
        <p class="card-description">a binary tool for Laplace-Dirichlet rule-based cardiac fiber generation</p>
        <img class="card-figure" src="images/fibers/la.png"></img>
        <div class="card-footer">
          <a class="click-icon fa-solid fa-file-lines tooltip" href="https://doi.org/10.1186/s12859-023-05260-w">
            <span class="tooltiptext">release paper</span>
          </a>
          <a class="click-icon fa-solid fa-download tooltip" href="https://doi.org/10.5281/zenodo.7622070">
            <span class="tooltiptext">download binaries and examples</span>
          </a>
          <a class="click-icon fa-solid fa-ellipsis tooltip" href="fibers.html" style="position: absolute; right: 20px">
            <span class="tooltiptext">read more</span>
          </a>
        </div>
      </div>

      <div class="card right">
        <p class="card-header">life<sup>x</sup>-ep</p>
        <p class="card-description">a binary tool for simulating cardiac electrophysiology</p>
        <img class="card-figure" src="images/ep/lv.png"></img>
        <div class="card-footer">
          <a class="click-icon fa-solid fa-file-lines tooltip" href="https://doi.org/10.1186/s12859-023-05513-8">
            <span class="tooltiptext">release paper</span>
          </a>
          <a class="click-icon fa-solid fa-download tooltip" href="https://doi.org/10.5281/zenodo.8085266">
            <span class="tooltiptext">download binaries and examples</span>
          </a>
          <a class="click-icon fa-solid fa-ellipsis tooltip" href="ep.html" style="position: absolute; right: 20px">
            <span class="tooltiptext">read more</span>
          </a>
        </div>
      </div>
    </div>

    <a name="citing-lifex"></a>
    <h1>Citing life<sup>x</sup></h1>

    <p>
      If you use life<sup>x</sup> in your research, please cite the associated release paper. Click <a
        href="cite-lifex.bib">here</a> to download the BibTeX database of life<sup>x</sup> publications.
    </p>

    <p>
      We maintain a list of papers using life<sup>x</sup> on the <a
        href="https://lifex.gitlab.io/lifex-public/publications.html">publications</a> page. If you would like to add
      your own, get in touch!
    </p>

    <a name="contacts"></a>
    <h1>Contacts</h1>

    For questions and suggestions, contact us <a href="mailto:michele.bucelli@polimi.it">by email</a> or by
    <a href="https://gitlab.com/lifex/lifex-public/-/issues/new">opening an issue on GitLab</a>.

    <footer id="footer">
      <div style="display: flex; align-items: center;">
        <img src="images/iheart-logo.png">
        <span>
          This project has received funding from the European Research Council (ERC) under the European Union's Horizon
          2020 research and innovation programme. Grant agreement No. 740132: iHEART - An Integrated Heart Model for the
          simulation of the cardiac function, P.I. Prof. A. Quarteroni. life<sup>x</sup> is developed and maintained as
          part of the iHEART Simulator.
        </span>
      </div>
    </footer>
  </div>
</body>

</html>