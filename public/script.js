function enlargeImage(img) {
  const modal = document.createElement('div');
  modal.classList.add('gallery-modal');

  const enlargedImg = document.createElement('img');
  enlargedImg.src = img.src;
  enlargedImg.classList.add('enlarged-image');

  const caption = document.createElement('span');
  caption.innerHTML = img.nextElementSibling.innerHTML;
  caption.classList.add('enlarged-caption');

  modal.appendChild(enlargedImg);
  modal.appendChild(caption);
  document.body.appendChild(modal);

  modal.onclick = function(event) {
    if (event.target === modal) {
      document.body.removeChild(modal);
    }
  };
}